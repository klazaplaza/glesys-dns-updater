import { Domain, DomainRecord } from "../models/glesys";
import axios from "axios";
import { GlesysSettings } from "../models/settings";

class Glesys {
  private username: string;
  private apiKey: string;
  private baseUrl: string;

  constructor(settings: GlesysSettings) {
    this.username = settings.username;
    this.apiKey = settings.apiKey;
    this.baseUrl = settings.apiBaseUrl;
  }

  async GetDomains(): Promise<Domain[]> {
    const response = await axios.get(`${this.baseUrl}/domain/list`, {
      auth: {
        username: this.username,
        password: this.apiKey,
      },
    });

    if (response.status !== 200) throw new Error(`Non-200 response: ${response.statusText}`);

    return response.data.response.domains as Domain[];
  }

  async GetRecords(domain: string): Promise<DomainRecord[]> {
    const response = await axios.post(
      `${this.baseUrl}/domain/listrecords`,
      {
        domainname: domain,
      },
      {
        auth: {
          username: this.username,
          password: this.apiKey,
        },
      }
    );

    if (response.status !== 200) throw new Error(`Non-200 response:${response.statusText}`);

    return response.data.response.records as DomainRecord[];
  }

  async UpdateRecord(recordId: Number, newIp: string): Promise<void> {
    const response = await axios.post(`${this.baseUrl}/domain/updaterecord`,
      {
        recordid: recordId,
        data: newIp,
      },
      {
        auth: {
          username: this.username,
          password: this.apiKey,
        },
      }
    );

    if (response.status !== 200) throw new Error(`Non-200 response: ${response.statusText}`);
  }
}

export { Glesys };
