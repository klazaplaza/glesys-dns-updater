import { MailSettings } from "../models/settings";
import nodemailer from "nodemailer";
import Mail from "nodemailer/lib/mailer";

export type Email = {
  subject: string;
  body: string;
  to: string[];
  from?: string;
};

export class Mailer {
  settings: MailSettings;
  transport: Mail;

  constructor(settings: MailSettings) {
    this.settings = settings;
    this.transport = nodemailer.createTransport(settings.transportConf);
  }

  async sendMail(mail: Email): Promise<void> {
    await this.transport.sendMail({
      from: mail.from ?? this.settings.defaultFrom,
      subject: mail.subject,
      to: mail.to,
      text: mail.body,
    });
  }
}
