import { AppSettings } from "../models/settings";
import axios from "axios";
import { Glesys } from "./Glesys";
import { Mailer } from "./Mailer";
import { DomainRecord } from "../models/glesys";
import { Log } from "../models";

class Updater {
	private settings: AppSettings;
	private glesys: Glesys;
	private mailer: Mailer;
	private log: Log;
	interval: NodeJS.Timeout | undefined;

	constructor(settings: AppSettings, log: Log) {
		this.settings = settings;
		this.glesys = new Glesys(settings.glesysSettings);
		this.mailer = new Mailer(settings.mailSettings);
		this.log = log;
	}

	async Start(updateIntervalInSeconds: number = 300): Promise<void> {
		try {
			this.log.info("Running first time after start");
			await this.Run();
		} catch (err) {
			this.log.error("Failed to run first time after start: " + err.message);

			await this.mailer.sendMail({
				to: this.settings.notificationEmails,
				subject: 'Failed to run updater',
				from: this.settings.mailSettings.defaultFrom,
				body: `Failed to run the updater, will try again in ${updateIntervalInSeconds} seconds. Reason:  ${err.message}`,
			})
		}

		this.log.info(`Starting interval, will try to run every ${updateIntervalInSeconds} seconds`);

		this.interval = setInterval(async () => {
			try {
				this.log.info("Running...");
				await this.Run();
			} catch (err) {
				this.log.error(`Failed to run: ${err.message}`);

				await this.mailer.sendMail({
					to: this.settings.notificationEmails,
					subject: 'Failed to run updater',
					from: this.settings.mailSettings.defaultFrom,
					body: `Failed to run the updater, will try again in ${updateIntervalInSeconds} seconds. Reason:  ${err.message}`,
				})
			}
		}, updateIntervalInSeconds * 1000);
	}

	Stop(): void {
		if (this.interval) {
			clearInterval(this.interval);
		}
	}

	async GetPublicIp(): Promise<string> {
		const response = await axios.get("https://ip.blurg.se");

		if (response.status !== 200) {
			throw new Error("Non-200 status returned: " + response.statusText);
		}

		return response.data;
	}

	async Run(): Promise<void> {
		const publicIp = await this.GetPublicIp();

		let recordsToUpdate: DomainRecord[] = [];

		for (const domainName in this.settings.glesysSettings.domains) {
			const records = await this.glesys.GetRecords(domainName);
			const aRecords = records.filter((n) => n.type === "A");
			const ru = aRecords
				.filter(r => this.settings.glesysSettings.domains[domainName].indexOf(r.host) > -1)
				.filter(r => r.data !== publicIp);

			recordsToUpdate = [...recordsToUpdate, ...ru];
		}

		if (!recordsToUpdate.length) {
			this.log.info('No records need updating, all is well in the world');
			return;
		}

		await this.sendMail(recordsToUpdate);

		for (const r of recordsToUpdate) {
			try {
				this.log.info(`Updating record ${r.host}.${r.domainname}`);
				await this.glesys.UpdateRecord(r.recordid, publicIp);
			} catch (e) {
				this.log.error("Failed to update record: " + JSON.stringify(r) + ' - ' + e.message);
				throw e;
			}
		}
	}

	async sendMail(recordsToUpdate: DomainRecord[]): Promise<void> {
		const domainList = recordsToUpdate.map(n => `${n.host}.${n.domainname}`).join('\n');

		await this.mailer.sendMail({
			to: this.settings.notificationEmails,
			subject: 'External ip changed',
			from: this.settings.mailSettings.defaultFrom,
			body: `The external ip has changed, please check your settings. Will try to upate the settins for the following domains: \n${domainList}`,
		})
	}
}

export { Updater };
