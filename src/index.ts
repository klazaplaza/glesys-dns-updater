import { Updater } from "./services/Updater";
import { AppSettings } from "./models/settings";
import * as appSettings from "./settings.json";

// Hnnnngnngngnnggnghhhhh...
const settings: AppSettings = (appSettings as unknown) as AppSettings;

// Poor mans logging
const log = {
	debug: (msg: string): void => console.debug(`DEB: ${new Date().toISOString()} - ${msg}`),
	info: (msg: string): void => console.info(`INF: ${new Date().toISOString()} - ${msg}`),
	error: (msg: string): void => console.error(`ERR: ${new Date().toISOString()} - ${msg}`)
}

const updater = new Updater(settings, log);

updater.Start().then(() => {
	log.info('Updater started!');
});

process.on('SIGTERM', () => {
	updater.Stop();
	process.exit(0);
})

process.on('SIGINT', () => {
	updater.Stop();
	process.exit(0);
});
