export type AppSettings = {
  mailSettings: MailSettings;
  glesysSettings: GlesysSettings;
  notificationEmails: string[];
};

export type MailSettings = {
  transportConf: string;
  defaultFrom: string;
};

export type GlesysSettings = {
  username: string;
  apiKey: string;
  apiBaseUrl: string;
  domains: Record<string, string[]>;
};
