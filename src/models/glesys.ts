export type Domain = {
  domainname: string;
  createtime: Date;
  displayname: string;
  recordcount: Number;
  registrarinfo: RegistrarInfo;
};

export type RegistrarInfo = {
  state: string;
  expire: Date;
  autorenew: string;
  tld: string;
};

export type DomainRecord = {
  recordid: Number;
  domainname: string;
  host: string;
  type: string;
  data: string;
  ttl: Number;
};
