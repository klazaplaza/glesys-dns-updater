# Glesys dns updater

Update dns records based on your external ip, just like dyn dns!

## Todo:
* Unit and integration tests
* Better logging and error handling
* Automatic builds and deploys

## Deploy

Clone repo. Make a settings.json with corret values in src.

Build docker file
```
docker build -t glesys-dns-updater:latest .
```

Run docker file
```
docker run -dit --name dns-updater --restart unless-stopped glesys-dns-updater:latest
```